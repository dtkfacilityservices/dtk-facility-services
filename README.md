DTK Facility Services specializes in providing janitorial and cleaning services that include floor care, window washing, landscaping, and disinfecting/sanitation to large facilities like schools, churches, and commercial buildings.

Address: 9821 Katy Fwy, Suite 500, Houston, TX 77024, USA

Phone: 713-463-7878

Website: https://dtkinc.com
